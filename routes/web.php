<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/index', function () {
//     return view('index');
// });
Route::get('/about-me', function () {
    return view('about-me');
});
Route::get('/dashboard', function () {
    return view('backend.dashboard');
});
// Route::post('/comments/{id}', [CommentController::class, 'store'])

Route::get('/index', [HomeController::class, 'index'])->name('index');


Route::group(['prefix'=>'roles','as'=>'roles.'], function(){
    Route::get('/', [RoleController::class, 'index'])->name('index');
    Route::get('/create', [RoleController::class, 'create'])->name('create');
    Route::post('/store', [RoleController::class, 'store'])->name('store');
    Route::get('/show/{role}', [RoleController::class, 'show'])->name('show');
    Route::get('/{role}/edit', [RoleController::class, 'edit'])->name('edit');
    Route::patch('/{role}', [RoleController::class, 'update'])->name('update');
});




// Route::get('/roles', [RoleController::class, 'index'])->name('roles.index');
// Route::get('/roles/create', [RoleController::class, 'create'])->name('roles.create');
// Route::post('/roles/store', [RoleController::class, 'store'])->name('roles.store');
// Route::get('/roles/show/{role}', [RoleController::class, 'show'])->name('roles.show');
// Route::get('/roles/{role}/edit', [RoleController::class, 'edit'])->name('roles.edit');
// Route::patch('/roles/{role}', [RoleController::class, 'update'])->name('roles.update');





// Route::resource('roles', RoleController::class);