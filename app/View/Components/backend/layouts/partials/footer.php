<?php

namespace App\View\Components\backend\layouts\partials;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class footer extends Component
{
    /**
     * Create a new component instance.
     */
    public $webName;


    public function __construct($webName)
    {
        $this->webName = $webName;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $web_name = $this->webName;
        return view('components.backend.layouts.partials.footer', compact('web_name'));
    }
}
